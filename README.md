# FoundryVTT DND5E 中文系统

本MOD为FVTT的DND5E系统加入了中文。

**使用说明：**

* 1. 首先在安装界面MOD安装中，安装本MOD。（ Manifest: https://gitlab.com/xtlcme/foundry-vtt-dnd5e-chn/raw/master/dnd5e_cn/module.json ）
* 2. 进入游戏世界（DND5E），在【设置-管理MOD】中勾选本MOD。
* 3. 在【设置-语言】中选择【中文】。
* 4. 重新载入

**关于合集包汉化的使用说明：**

* 1.安装Babele（ Manifest: https://gitlab.com/riccisi/foundry-vtt-babele/raw/master/module/module.json ）
* 2.在设置-MOD管理中勾选开启Babele
* 3.配置设定-MOD设定-Babele中指定路径（modules/dnd5e_cn/compendium）点击左边的Select，保存更改

**手动安装方法**

* 1.在右上方选择Download-zip
* 2.解压文件，将dnd5e_chn拷贝到Data/Module
* 3.从使用说明第二步开始

**翻译协力人员：**

* 常乐
* 白菜他爸
* 青日

感谢诸位的协力！
遇到有什么问题可以到QQ群：716573917 交流。



